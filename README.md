Implementation of Symmetric Explanation Learning (SEL) in Glucose 4.0
=====================================================================

Directory overview:
-------------------

`core/` A core version of the solver glucose (no main here)  
`experiments/` An extended solver with simplification capabilities  
`mtl/` MiniSat Template Library  
`parallel/` A multicore version of glucose **! Not compatible with SEL ! **  
`simp/` An extended solver with simplification capabilities  
`testfiles/` Some test cnfs with a corresponding symmetry file  
`utils/` MiniSat util files  
`README`  
`LICENCE`  
`Changelog`  

To build (release version: without assertions, statically linked, etc):
-----------------------------------------------------------------------

`cd simp`  
`make rs`  
(Like MiniSat...)  

Usage:
------

info: `./glucose --help`  
run: `./glucose testfiles/holes/hole002.cnf`  
run with symmetry: `./glucose testfiles/holes/hole002.cnf` (Glucose automatically  searches for the file `testfiles/holes/hole002.cnf.sym`  

Branches:
---------

`master`: master branch with implementation of SEL  
`sym-learning-scheme`: branch with implementation of Symmetrical Learning Scheme (SLS)  
`full-intch-prop`: experimental branch  
`strongMinimization`: experimental branch  

Cite as:
--------
```
@InProceedings{10.1007/978-3-319-66263-3_6,
author="Devriendt, Jo
and Bogaerts, Bart
and Bruynooghe, Maurice",
editor="Gaspers, Serge
and Walsh, Toby",
title="Symmetric Explanation Learning: Effective Dynamic Symmetry Handling for {SAT}",
booktitle="Theory and Applications of Satisfiability Testing -- SAT 2017",
year="2017",
publisher="Springer International Publishing",
address="Cham",
pages="83--100",
abstract="The presence of symmetry in Boolean satisfiability (SAT) problem instances often poses challenges to solvers. Currently, the most effective approach to handle symmetry is by static symmetry breaking, which generates asymmetric constraints to add to the instance. An alternative way is to handle symmetry dynamically during solving. As modern SAT solvers can be viewed as propositional proof generators, adding a symmetry rule in a solver's proof system would be a straightforward technique to handle symmetry dynamically. However, none of these proposed symmetrical learning techniques are competitive to static symmetry breaking. In this paper, we present symmetric explanation learning, a form of symmetrical learning based on learning symmetric images of explanation clauses for unit propagations performed during search. A key idea is that these symmetric clauses are only learned when they would restrict the current search state, i.e., when they are unit or conflicting. We further provide a theoretical discussion on symmetric explanation learning and a working implementation in a state-of-the-art SAT solver. We also present extensive experimental results indicating that symmetric explanation learning is the first symmetrical learning scheme competitive with static symmetry breaking.",
isbn="978-3-319-66263-3"
}
```


